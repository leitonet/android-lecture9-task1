package com.example.l9task1;


import java.io.Serializable;
import java.util.List;

public class Country implements Serializable {
    private String name;
    private String capital;
    private long population;
 //   private String borders;
 //   private String timezone;
    private String region;
    private String subRegion;
    private String alpha2Code;
    List<Country> countries;

    public Country(String name, String capital, long population, /*String[] borders, String [] timezone,*/ String region, String subRegion, String alpha2Code) {

        this.name = name;
        this.capital = capital;
        this.population = population;
  /*      for(int i = 0; i < timezone.length; i++) {
            this.timezone = this.timezone + timezone[i] + ", ";
        }
        for(int i = 0; i < borders.length; i++) {
            this.borders = this.borders + borders[i] + ", ";
        }*/
        this.region = region;
        this.subRegion = subRegion;
        this.alpha2Code = alpha2Code;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public long getPopulation() {
        return population;
    }

    /*public String getBorders() {
        return borders;
    }

    public String getTimezone() {
        return timezone;
    }*/

    public String getRegion() {
        return region;
    }

    public String getSubRegion() {
        return subRegion;
    }

    public String getAlpha2Code() {
        return alpha2Code;
    }

}
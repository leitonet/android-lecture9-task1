package com.example.l9task1;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import static com.example.l9task1.MainActivity.KEY;


public class CountryActivity extends AppCompatActivity {

    TextView name;
    TextView capital;
    TextView population;
    TextView borders;
    TextView timezone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_activity);
        name = (TextView) findViewById(R.id.name);
        capital = (TextView) findViewById(R.id.capital);
        population = (TextView) findViewById(R.id.population);
        borders = (TextView) findViewById(R.id.borders);
        timezone = (TextView) findViewById(R.id.timezone);

        if(getIntent()!= null){
            Country country = (Country)getIntent().getSerializableExtra(KEY);
            name.setText(country.getName());
            capital.setText("Country capital is - " + country.getCapital());
            population.setText("Country population is - " + country.getPopulation());
            /*borders.setText("Country borders is - " + country.getBorders());
            timezone.setText("Country timezone is - " + country.getTimezone());*/
        }
    }
}
